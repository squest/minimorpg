# MiniMORPG
[![pipeline status](https://gitlab.com/Squest/minimorpg/badges/master/pipeline.svg)
](https://gitlab.com/Squest/minimorpg/commits/master)
[![JavaDoc](https://img.shields.io/badge/JavaDoc-latest-orange.svg)](https://squest.gitlab.io/minimorpg)

## About the project
MiniMORPG is a RPG inspired by the Chrono Trigger, The Legend of Zelda and Mario&Luigi games. The game aims to be
playable in singleplayer offline or in multiplayer online or on the local network. It provides an API so that any server
can create its own custom story with new quests, characters, items, maps, etc. using plugins (server side) and mods 
(client side).
## Building
In order to build the game, you need to launch the following command:
```bash
mvn compile assembly:single
```
Then you can find the compiled jar in the `out/` directory with a name of the form
`MiniMORPG-{version}-jar-with-dependencies`.
## Dependencies
- Maven (required to build from source and to build the doc)
#### Maven Dependencies
- Gson (2.8.5) - Json parser by Google
- Brigadier (1.0.17) - Command Interpreter by Mojang

You do **not** need to install them manually, Maven does it for you.
## Licensing
This project is distributed under the MIT License. See [LICENSE](LICENSE) for further details.