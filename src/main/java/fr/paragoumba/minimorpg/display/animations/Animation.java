package fr.paragoumba.minimorpg.display.animations;

import java.util.List;

public abstract class Animation {

    public Animation(String id){

        this.id = id;

    }

    protected String id;

    public String getId() {

        return id;

    }

    public abstract String getActualFrameName();

    public abstract List<String> getAllFramesIds();

}
