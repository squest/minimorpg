package fr.paragoumba.minimorpg.display.animations;

import fr.paragoumba.minimorpg.display.Frame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DynamicAnimation extends Animation {

    public DynamicAnimation(String id, List<Frame> frames) {

        super(id);

        this.frames = frames;

        lastFrame = 0;

    }

    public DynamicAnimation(String id, Frame... frames) {

        this(id, Arrays.asList(frames));

    }

    private List<Frame> frames;
    private long lastTimestamp = System.currentTimeMillis();
    private int lastFrame;

    @Override
    public String getActualFrameName(){

        int lastFrameDuration = frames.get(lastFrame).getFrameDuration();

        if (System.currentTimeMillis() - lastTimestamp >= lastFrameDuration){

            lastFrame = lastFrame == frames.size() - 1 ? 0 : lastFrame + 1;
            lastTimestamp = System.currentTimeMillis();

            return frames.get(lastFrame).getFrameId();

        }

        return frames.get(lastFrame).getFrameId();

    }

    @Override
    public List<String> getAllFramesIds(){

        List<String> framesIds = new ArrayList<>();

        for (Frame frame : frames){

            framesIds.add(frame.getFrameId());

        }

        return framesIds;

    }
}
