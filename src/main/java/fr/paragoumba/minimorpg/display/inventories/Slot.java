package fr.paragoumba.minimorpg.display.inventories;

import fr.paragoumba.minimorpg.ItemStack;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Slot extends InventoryComponent {

    public Slot(){

        this(0, 0);

    }

    public Slot(int relativeX, int relativeY){

        this(relativeX, relativeY, 18, 18);

    }

    public Slot(int relativeX, int relativeY, int width, int height){

        super(relativeX, relativeY, width, height);

    }

    public Slot(BufferedImage backgroundImage){

        this();
        this.backgroundImage = backgroundImage;

    }

    private ItemStack itemStack;
    private BufferedImage backgroundImage;

    @Override
    public void render(Graphics g){

        g.setColor(new Color(155, 148, 92));
        g.fillRect(0, 0, width - resolution, resolution);
        g.fillRect(0, resolution, resolution, height - 2 * resolution);

        g.setColor(new Color(197, 188, 118));
        g.fillRect(resolution, resolution, width - 2 * resolution, height - 2 * resolution);

        g.setColor(new Color(173, 167, 122));
        g.fillRect(width - resolution, 0, resolution, resolution);
        g.fillRect(0, height - resolution, resolution, resolution);

        //g.setColor(new Color(202, 194, 135));
        //g.setColor(new Color(204, 197, 141));
        g.setColor(new Color(206, 200, 147));
        g.fillRect(width - resolution, resolution, resolution, height - 2 * resolution);
        g.fillRect(resolution, height - resolution, width - resolution, resolution);

        if (backgroundImage != null){

            g.drawImage(
                    backgroundImage,
                    resolution,
                    resolution,
                    width - 2 * resolution,
                    height - 2 * resolution,
                    null
            );
        }

        super.render(g);

    }

    @Override
    public int getWidth(){

        return width;

    }

    @Override
    public int getHeight(){

        return height;

    }

    public void setItemStack(ItemStack itemStack){

        this.itemStack = itemStack;

    }
}
