package fr.paragoumba.minimorpg.display.inventories;

import java.awt.*;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;

public class Group<T extends InventoryComponent> extends InventoryComponent {

    public Group(Class<T> componentClass, int rows, int columns){

        components = (T[][]) Array.newInstance(componentClass, rows, columns);

        int x = 0, y = 0;
        int maxWidth = 0;

        for (T[] componentsRow : components){

            int maxRowHeight = 0;

            for (int j = 0; j < componentsRow.length; ++j){

                try{

                    componentsRow[j] = componentClass
                            .getConstructor(int.class, int.class)
                            .newInstance(x, y);

                    x += componentsRow[j].getWidth();
                    int height = componentsRow[j].getHeight();

                    if (height > maxRowHeight){

                        maxRowHeight = height;

                    }

                } catch (InstantiationException | IllegalAccessException |
                        NoSuchMethodException | InvocationTargetException e){

                    e.printStackTrace();

                }
            }

            if (x > maxWidth){

                maxWidth = x;

            }

            x = 0;
            y += maxRowHeight;

        }

        width = maxWidth;
        height = y;

    }

    private T[][] components;

    @Override
    public void render(Graphics g){

        System.out.println(g.getClipBounds());

        int x = relativeX, y = relativeY;

        for (T[] componentsRow : components){

            int maxHeight = 0;

            for (T component : componentsRow){

                int width = component.getWidth();
                int height = component.getHeight();

                component.render(g.create(x, y, width, height));

                x += width;

                if (height > maxHeight){

                    maxHeight = height;

                }
            }

            x = relativeX;
            y += maxHeight;

        }

        System.out.println("Rendering Group " + components.length);

    }
}
