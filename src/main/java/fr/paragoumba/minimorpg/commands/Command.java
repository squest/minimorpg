package fr.paragoumba.minimorpg.commands;

public class Command {

    private StringNode commandName;

    public StringNode getCommandName() {

        return commandName;

    }

    public StringNode forStr(String commandName){

        return this.commandName = new StringNode(commandName);

    }

    public static Node argumentStr(String name){

        return new StringNode(name);

    }

    public static Node argumentInt(String name){

        return new IntNode(name);

    }

    public static Node argumentFloat(String name){

        return new FloatNode(name);

    }
}
