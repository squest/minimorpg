package fr.paragoumba.minimorpg.commands;

public interface CommandAction {

    boolean onCommand(Command command);

}
