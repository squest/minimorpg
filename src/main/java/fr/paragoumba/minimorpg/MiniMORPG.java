package fr.paragoumba.minimorpg;

import fr.paragoumba.minimorpg.game.GameManager;

import java.util.Properties;

public class MiniMORPG {

    public static String APP_NAME;
    public static String VERSION;

    public static void main(String[] args) {

        try{

            Properties properties = new Properties();
            properties.load(MiniMORPG.class.getResourceAsStream("/project.properties"));

            APP_NAME = properties.getProperty("artifactId");
            VERSION = properties.getProperty("version");

        } catch (Exception e) {

            APP_NAME = "UNKNOWN";
            VERSION = "UNKNOWN";

        }

        GameManager.init();

    }
}