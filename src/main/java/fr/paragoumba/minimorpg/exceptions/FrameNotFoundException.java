package fr.paragoumba.minimorpg.exceptions;

public class FrameNotFoundException extends RenderException {

    public FrameNotFoundException(String frameName, String animationId){

        super("Frame '" + frameName + "' of animation '" + animationId + "' cannot be found.");

    }
}