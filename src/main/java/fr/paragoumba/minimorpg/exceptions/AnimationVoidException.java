package fr.paragoumba.minimorpg.exceptions;

public class AnimationVoidException extends RenderException {

    public AnimationVoidException(String message) {

        super(message);

    }
}
