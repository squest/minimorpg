package fr.paragoumba.minimorpg.exceptions;

public abstract class RenderException extends Exception {

    public RenderException(String message){

        super(message);

    }
}
