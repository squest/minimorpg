package fr.paragoumba.minimorpg.exceptions;

public class AnimationNotFoundException extends RenderException {

    public AnimationNotFoundException(String message){

        super("Animation " + message + " doesn't exists.");

    }
}
