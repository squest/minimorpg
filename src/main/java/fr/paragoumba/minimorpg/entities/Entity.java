package fr.paragoumba.minimorpg.entities;

import fr.paragoumba.minimorpg.game.GameManager;
import fr.paragoumba.minimorpg.commands.CommandSource;
import fr.paragoumba.minimorpg.display.animations.Animation;
import fr.paragoumba.minimorpg.exceptions.FrameNotFoundException;
import fr.paragoumba.minimorpg.exceptions.RenderException;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Entity implements CommandSource {

    public Entity(int x, int y, EntityType type, String defaultAnimationId) {

        this.x = x;
        this.y = y;
        this.type = type;
        actualAnimationId = defaultAnimationId;

    }

    protected EntityType type;
    protected String actualAnimationId;
    protected int x;
    protected int y;
    protected int life;

    public int getX() {

        return x;

    }

    public int getY() {

        return y;

    }

    public int getWidth() throws RenderException {

        return getActualTile().getWidth();

    }

    public int getHeight() throws RenderException {

        return getActualTile().getHeight();

    }

    public BufferedImage getActualTile() throws RenderException {

        Animation animation = type.getAnimation(actualAnimationId);
        BufferedImage frame = type.getActualFrame(animation);

        if (frame == null){

            throw new FrameNotFoundException(animation.getActualFrameName(), actualAnimationId);

        }

        return frame;

    }

    public abstract void render(Graphics2D g2d) throws RenderException;

    public abstract int getHitboxX();
    public abstract int getHitboxY();

    public abstract int getHitboxWidth();
    public abstract int getHitboxHeight();

    public void setAnimation(String animationId) {

        this.actualAnimationId = animationId;

    }

    public void drawHitbox(Graphics g){

        g.setColor(Color.RED);
        g.drawRect(getHitboxX(), getHitboxY(), getHitboxWidth() - 1, getHitboxHeight() - 1);

    }

    public void move(int offsetX, int offsetY) {

        if (!GameManager.game.hit(this, 0, 0)){
            if (GameManager.game.hit(this, offsetX, offsetY)){

                return;

            }
        }

        this.x += offsetX;
        this.y += offsetY;

    }

    public void tp(int x, int y){

        this.x = x;
        this.y = y;

        System.out.println("Tped to " + x + ':' + y);

    }
}
