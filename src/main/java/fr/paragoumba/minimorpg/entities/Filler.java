package fr.paragoumba.minimorpg.entities;

import java.awt.*;

public class Filler extends Particle {

    public Filler(int x, int y, int width, int height) {

        super(x, y, null, null);

        this.width = width;
        this.height = height;

    }

    private int width;
    private int height;

    @Override
    public void render(Graphics2D g2d) {

        g2d.setColor(Color.BLUE);

        for (int i = 0; i < width; ++i)
            for (int j = (int) (Math.random() * height); j < height; ++j)
                g2d.fillRect(x + i - width / 2, y + j - height / 2, 1, 1);

    }

    @Override
    public int getHitboxX() {

        return x - width / 2;

    }

    @Override
    public int getHitboxY() {

        return y - height / 2;

    }

    @Override
    public int getHitboxWidth() {

        return width;

    }

    @Override
    public int getHitboxHeight() {

        return height;

    }
}
