package fr.paragoumba.minimorpg.entities;

public class NPC extends Character {

    public NPC(int x, int y, String name, EntityType type, String defaultAnimationId) {

        super(x, y, name, type, defaultAnimationId);

    }
}
