package fr.paragoumba.minimorpg;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.Map.Entry;

public class KeyboardHandler implements KeyListener {

    private static HashMap<Integer, Entry<Boolean, Long>> pressedKeys = new HashMap<>();

    @Override
    public void keyTyped(KeyEvent keyEvent) {}

    @Override
    public void keyPressed(KeyEvent keyEvent) {

        pressedKeys.put(keyEvent.getKeyCode(), new SimpleEntry<>(true, System.currentTimeMillis()));

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {

        pressedKeys.put(keyEvent.getKeyCode(), new SimpleEntry<>(false, System.currentTimeMillis()));

    }

    public static boolean isKeyPressed(int keyCode){

        Entry<Boolean, Long> entry = pressedKeys.get(keyCode);

        return entry != null && entry.getKey();

    }

    public static long getKeyTimestamp(int keyCode){

        Entry<Boolean, Long> entry = pressedKeys.get(keyCode);

        return entry != null ? entry.getValue() : -1;

    }
}
