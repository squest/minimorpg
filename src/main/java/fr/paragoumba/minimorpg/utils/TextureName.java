package fr.paragoumba.minimorpg.utils;

public class TextureName {

    public TextureName(String entityId, String textureId){

        this.entityId = entityId;
        this.textureId = textureId;

    }

    private String entityId;
    private String textureId;

    public String getFullTextureName(){

        return entityId + '/' + textureId;

    }

    public String getTextureId(){

        return textureId;

    }

    public String getEntityId(){

        return entityId;

    }
}
