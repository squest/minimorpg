package fr.paragoumba.minimorpg.game;

import com.google.gson.Gson;
import fr.paragoumba.minimorpg.entities.EntityHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class GameManager {

    private GameManager(){}

    public static Game game;

    public static void init(){

        game = new Game();

        game.init();
        game.loop();

    }

    public static boolean loadGame(File saveFile){

        return false;

    }

    public static void saveGame(File saveFile){

        HashMap<String, Object> map = new HashMap<>();

        map.put("entities", EntityHandler.lockEntities());
        map.put("players", EntityHandler.lockPlayers());

        Gson gson = new Gson();

        try(FileOutputStream fos = new FileOutputStream(saveFile)){

            fos.write(gson.toJson(map).getBytes());

        } catch (IOException e) {

            e.printStackTrace();

        }

        EntityHandler.unlockEntities();
        EntityHandler.unlockPlayers();

    }
}
