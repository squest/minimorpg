package fr.paragoumba.minimorpg.game;

import fr.paragoumba.minimorpg.Collisions;
import fr.paragoumba.minimorpg.KeyboardHandler;
import fr.paragoumba.minimorpg.ResourceManager;
import fr.paragoumba.minimorpg.display.Chat;
import fr.paragoumba.minimorpg.display.Window;
import fr.paragoumba.minimorpg.entities.Character;
import fr.paragoumba.minimorpg.entities.*;

import java.awt.event.KeyEvent;
import java.util.Comparator;
import java.util.Timer;
import java.util.TimerTask;

import static fr.paragoumba.minimorpg.MiniMORPG.APP_NAME;
import static fr.paragoumba.minimorpg.MiniMORPG.VERSION;

public class Game {

    public static boolean running = true;
    private static int frames = 0;

    private Window window;
    private long lastChatToggle = 0;

    void init(){

        ResourceManager.init();
        EntityType.init();
        EntityHandler.init();

        window = new Window();

        Player player = new Player(
                200,
                200,
                "Snowman",
                EntityType.getEntityType(EntityType.SNOWMAN),
                "default"
        );

        EntityHandler.setPlayer(player);
        EntityHandler.addEntity(player);

        EntityHandler.addEntity(new Character(1000, 500, "Test", EntityType.getEntityType(EntityType.MARIO), "default"));

        EntityHandler.addEntity(
                new Character(
                        150,
                        150,
                        "Mario",
                        EntityType.getEntityType(EntityType.MARIO),
                        "default"
                )
        );

        EntityHandler.addEntity(
                new Character(
                        500,
                        500,
                        "Charizard",
                        EntityType.getEntityType(EntityType.CHARIZARD),
                        "default"
                )
        );

        EntityHandler.addEntity(
                new Filler(
                        50,
                        50,
                        100,
                        100
                )
        );

        Timer time = new Timer();

        time.scheduleAtFixedRate(new TimerTask() {

            @Override
            public void run() {

                window.setTitle(APP_NAME + " - v" + VERSION + " - " + frames + "FPS");
                frames = 0;

            }
        }, 1000, 1000);

    }

    public Window getWindow() {

        return window;

    }

    public void loop(){

        while (running){

            if (KeyboardHandler.isKeyPressed(KeyEvent.VK_ESCAPE)){

                System.exit(0);

            }

            update();

            EntityHandler.lockEntities().sort(Comparator.comparingInt(Entity::getY));
            EntityHandler.unlockEntities();

            render();

            try {

                Thread.sleep(16);

            } catch (InterruptedException e){

                e.printStackTrace();

            }
        }
    }

    public boolean hit(Entity entity, int offsetX, int offsetY){

        boolean flag = false;

        for (Entity otherEntity : EntityHandler.lockEntities()){
            if (entity != otherEntity &&
                    Collisions.entityHitEntityV2(entity, otherEntity, offsetX, offsetY)){

                flag = true;

            }

        }

        EntityHandler.unlockEntities();

        return flag;

    }

    public void render(){

        window.render();

    }

    public void update(){

        Chat chat = window.getChat();

        if (!chat.hasFocus()){

            Player player = EntityHandler.getPlayer();
            boolean walk = false;

            if (KeyboardHandler.isKeyPressed(KeyEvent.VK_Z)){

                player.setAnimation("walk");
                player.move(0, -2);
                walk = true;

            }

            if (KeyboardHandler.isKeyPressed(KeyEvent.VK_Q)){

                player.setAnimation("walk");
                player.move(-2, 0);
                walk = true;

            }

            if (KeyboardHandler.isKeyPressed(KeyEvent.VK_S)){

                player.setAnimation("walk");
                player.move(0, 2);
                walk = true;

            }

            if (KeyboardHandler.isKeyPressed(KeyEvent.VK_D)){

                player.setAnimation("walk");
                player.move(2, 0);
                walk = true;

            }

            if (!walk){

                player.setAnimation("default");

            }

            if (KeyboardHandler.isKeyPressed(KeyEvent.VK_T) && System.currentTimeMillis() - lastChatToggle > 200){

                chat.giveFocus(!chat.hasFocus());
                lastChatToggle = System.currentTimeMillis();

            }
        }
    }
}
